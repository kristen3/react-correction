import './style.css';

export default function Exemple ({ exemple1 , exemple2 }) {
    //Etat
    const [exemple, setExemple] = useState([]);

    //effet
    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/posts').then((res) => {
            setPosts(res.data);
        });
    }, []);

    
    // Affichage (rendu)
    return (
        <div className="post--card">
            <h2>{exemple1 }</h2>
            <p>{exemple2}</p>
        </div>
    );
}

// puis ajouter dans le document principale import Exemple from './components/Exemple/Exemple';
