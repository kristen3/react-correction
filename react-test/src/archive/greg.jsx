import { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';

import Post from './components/Post/Post';

function App() {
    //Etat
    const [posts, setPosts] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");

    //Effet
    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/posts').then((res) => {
            setPosts(res.data);
        });
    }, []);
    
    //filtre
    const filteredData = posts.filter(post => post.title.toLowerCase().includes(searchTerm.toLowerCase()));


    return (
        <div className="App">
            <h1>My blog posts</h1>y

            <input 
        type="text" 
        placeholder="Rechercher un Pokemon"
        value={searchTerm}
        onChange={e => setSearchTerm(e.target.value)}
      />
      {filteredData &&
        filteredData.map((post) => {
                    return <Post key={post.id} title={post.title} body={post.body} />;
                  })}
        </div>
    );
}

export default App;