import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import {postStore} from "../lib/store/postStore";


export function Edit() {
  
    const {posts, currentPost, setCurrentPost} = postStore();   
    const {id} = useParams();
    const [title, setTitle] = useState(currentPost.title);
    const [body, setBody] = useState(currentPost.body);

    useEffect(() => {
        const currentPost = posts.find((post)=> post.id === Number(id));
        setCurrentPost(currentPost);
        setTitle(currentPost.title);
        setBody(currentPost.body);
      }, []);

    const handleSubmit = (event) => {
        event.preventDefault();
        setCurrentPost({...currentPost, title, body});
    };

    const titleUpdate = (event) => {
        setTitle(event.target.value);
    };

    const bodyUpdate = (event) => {
        setBody(event.target.value);
    };

  return (
     <div>
        <form onSubmit={handleSubmit}>
            <input type="text" value={title} onChange={titleUpdate}/><br /><br />
            <input type="text" value={body} onChange={bodyUpdate}/>
            <button type="submit">Modifier</button>
        </form>
     </div>
  );
}